# rfc-dns
Make DNS RFCs up-to-date again

## Important warning

This project is at a very preliminary stage. Nothing substantial is
done yet. No real decision is taken. Do not expect anything.

## Goal

The idea is to explore the possibility of updating the official RFCs
for DNS, RFC 1034 and 1035.

## License
This repository relates to activities in the Internet Engineering Task
Force(IETF). All material in this repository is considered Contributions
to the IETF Standards Process, as defined in the intellectual property
policies of IETF currently designated as
[BCP 78](https://www.rfc-editor.org/info/bcp78),
[BCP 79](https://www.rfc-editor.org/info/bcp79) and the
[IETF Trust Legal Provisions (TLP) Relating to IETF Documents](http://trustee.ietf.org/trust-legal-provisions.html).

Any edit, commit, pull request, issue, comment or other change made to
this repository constitutes Contributions to the
[IETF Standards Process](https://www.ietf.org/).

You agree to comply with all applicable IETF policies and procedures,
including, BCP 78, 79, the TLP, and the TLP rules regarding code
components (e.g. being subject to a Simplified BSD License) in
Contributions.

